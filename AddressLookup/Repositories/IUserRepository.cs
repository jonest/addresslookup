﻿using System.Collections.Generic;
using System.Threading.Tasks;
using AddressLookup.BussinessObjects;

namespace AddressLookup.Repositories
{
   public interface IUserRepository
   {
      /// <summary>
      /// Returns all users in the data store
      /// </summary>
      /// <returns>All users in the data store</returns>
      IEnumerable<User> GetUsers();

      /// <summary>
      /// Searches user first and last names for matches
      /// </summary>
      /// <param name="searchText">The search text for user first and last names</param>
      /// <returns>All users matching the search text</returns>
      IEnumerable<User> SearchUsers( string searchText );

      /// <summary>
      /// Adds user to the database asynchronously
      /// </summary>
      /// <param name="user">The user to be added</param>
      /// <returns>A task representing the work</returns>
      Task<int> AddUserAsync( User user );

      /// <summary>
      /// Deletes a user from the database asynchronously
      /// </summary>
      /// <param name="userId">Id for the user to be deleted</param>
      /// <returns>A task representing the work</returns>
      Task DeleteUserAsync( string userId );
   }
}