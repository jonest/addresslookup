﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AddressLookup.BussinessObjects;
using AddressLookup.Models;

namespace AddressLookup.Repositories
{
   public class UserRepository : IUserRepository
   {
      public UserRepository()
      {
      }

      public IEnumerable<User> GetUsers()
      {
         using ( var context = new AddressLookupDbContext() )
         {
             List<User> users = context.Users.Select( User.FromUserEntity ).ToList();
             return users;
         }
      }

      public IEnumerable<User> SearchUsers( string searchText )
      {
         using ( var context = new AddressLookupDbContext() )
         {
            return
               context.Users.Where( user => user.FirstName.Contains( searchText ) || user.LastName.Contains( searchText ) )
                  .Select( User.FromUserEntity )
                  .ToList();
         }
      }

      public async Task<int> AddUserAsync( User user )
      {
         var addressEntity = new AddressEntity
         {
            Line1 = user.Address.Line1,
            Line2 = user.Address.Line2,
            City = user.Address.City,
            State = user.Address.State,
            Zip = user.Address.Zip
         };

         List<InterestEntity> interestEntities = user.Interests.Select( interest => new InterestEntity
         {
            Name = interest.Name
         } ).ToList();

         var userEntity = new UserEntity
         {
            FirstName = user.FirstName,
            LastName = user.LastName,
            Age = user.Age,
            PictureUrl = user.PictureUrl.AbsoluteUri,
            Address = addressEntity,
            Interests = interestEntities
         };

         UserEntity newUser;
         using ( var context = new AddressLookupDbContext() )
         {
            newUser = context.Users.Add( userEntity );

            await context.SaveChangesAsync().ConfigureAwait( false );
         }

         return newUser.UserId;
      }

      public async Task DeleteUserAsync( string userId )
      {
         int userIdAsInt = Convert.ToInt32( userId );
         using ( var context = new AddressLookupDbContext() )
         {
            UserEntity userEntity = context.Users.SingleOrDefault( user => user.UserId == userIdAsInt );

            if ( userEntity != null )
            {
               context.Users.Remove( userEntity );
               await context.SaveChangesAsync().ConfigureAwait( false );
            }
         }
      }
   }
}