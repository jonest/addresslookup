﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace AddressLookup.Specification.Interest
{
   public class InterestSpecification : CompositeSpecification<List<BussinessObjects.Interest>>
   {
      private readonly string _interest;

      public InterestSpecification( string interest )
      {
         _interest = interest;
      }

      public override bool IsSatisfiedBy( List<BussinessObjects.Interest> candidate )
      {
         if ( candidate == null || string.IsNullOrWhiteSpace( _interest ) )
         {
            return false;
         }

         return candidate.Any( i => !string.IsNullOrWhiteSpace( i.Name ) && i.Name.IndexOf( _interest, StringComparison.OrdinalIgnoreCase ) >= 2 );
      }
   }
}