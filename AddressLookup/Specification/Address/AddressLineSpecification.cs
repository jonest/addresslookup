﻿using System;

namespace AddressLookup.Specification.Address
{
   public class AddressLineSpecification : CompositeSpecification<BussinessObjects.Address>
   {
      private readonly string _line;

      public AddressLineSpecification( string line )
      {
         _line = line;
      }

      public override bool IsSatisfiedBy( BussinessObjects.Address candidate )
      {
         if ( candidate == null || string.IsNullOrWhiteSpace( _line ) || string.IsNullOrWhiteSpace( candidate.Line1 ) )
         {
            return false;
         }

         if ( candidate.Line1.IndexOf( _line, StringComparison.OrdinalIgnoreCase ) >= 2 )
         {
            return true;
         }

         if ( !string.IsNullOrWhiteSpace( candidate.Line2 ) )
         {
            return candidate.Line2.IndexOf( _line, StringComparison.OrdinalIgnoreCase ) >= 2;
         }

         return false;
      }
   }
}