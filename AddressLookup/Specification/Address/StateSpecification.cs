﻿using System;

namespace AddressLookup.Specification.Address
{
   public class StateSpecification : CompositeSpecification<BussinessObjects.Address>
   {
      private readonly string _state;

      public StateSpecification( string state )
      {
         _state = state;
      }

      public override bool IsSatisfiedBy( BussinessObjects.Address candidate )
      {
         if ( candidate == null || string.IsNullOrWhiteSpace( _state ) || string.IsNullOrWhiteSpace( candidate.State ) )
         {
            return false;
         }

         return candidate.State.IndexOf( _state, StringComparison.OrdinalIgnoreCase ) >= 0;
      }
   }
}