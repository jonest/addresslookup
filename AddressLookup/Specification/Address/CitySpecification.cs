﻿using System;

namespace AddressLookup.Specification.Address
{
   public class CitySpecification : CompositeSpecification<BussinessObjects.Address>
   {
      private readonly string _city;

      public CitySpecification( string city )
      {
         _city = city;
      }

      public override bool IsSatisfiedBy( BussinessObjects.Address candidate )
      {
         if ( candidate == null || string.IsNullOrWhiteSpace( _city ) || string.IsNullOrWhiteSpace( candidate.City ) )
         {
            return false;
         }

         return candidate.City.IndexOf( _city, StringComparison.OrdinalIgnoreCase ) >= 0;
      }
   }
}