﻿using System;

namespace AddressLookup.Specification.Address
{
   public class ZipSpecification : CompositeSpecification<BussinessObjects.Address>
   {
      private readonly string _zip;

      public ZipSpecification( string zip )
      {
         _zip = zip;
      }

      public override bool IsSatisfiedBy( BussinessObjects.Address candidate )
      {
         if ( candidate == null || string.IsNullOrWhiteSpace( _zip ) || string.IsNullOrWhiteSpace( candidate.Zip ) )
         {
            return false;
         }

         return candidate.Zip.IndexOf( _zip, StringComparison.OrdinalIgnoreCase ) >= 0;
      }
   }
}