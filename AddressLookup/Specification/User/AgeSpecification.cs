﻿namespace AddressLookup.Specification.User
{
   public class AgeSpecification : CompositeSpecification<BussinessObjects.User>
   {
      private readonly int _age;

      public AgeSpecification( int age )
      {
         _age = age;
      }

      public override bool IsSatisfiedBy( BussinessObjects.User candidate )
      {
         if ( candidate == null )
         {
            return false;
         }

         return candidate.Age == _age;
      }
   }
}