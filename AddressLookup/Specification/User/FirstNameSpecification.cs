﻿using System;

namespace AddressLookup.Specification.User
{
   public class FirstNameSpecification : CompositeSpecification<BussinessObjects.User>
   {
      private readonly string _firstName;

      public FirstNameSpecification( string firstName )
      {
         _firstName = firstName;
      }

      public override bool IsSatisfiedBy( BussinessObjects.User candidate )
      {
         if ( candidate == null || string.IsNullOrWhiteSpace( _firstName ) || string.IsNullOrWhiteSpace( candidate.FirstName ) )
         {
            return false;
         }

         return candidate.FirstName.IndexOf( _firstName, StringComparison.OrdinalIgnoreCase ) >= 0;
      }
   }
}