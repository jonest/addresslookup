﻿using System;

namespace AddressLookup.Specification.User
{
   public class LastNameSpecification : CompositeSpecification<BussinessObjects.User>
   {
      private readonly string _lastName;

      public LastNameSpecification( string lastName )
      {
         _lastName = lastName;
      }

      public override bool IsSatisfiedBy( BussinessObjects.User candidate )
      {
         if ( candidate == null || string.IsNullOrWhiteSpace( _lastName ) || string.IsNullOrWhiteSpace( candidate.LastName ) )
         {
            return false;
         }

         return candidate.LastName.IndexOf( _lastName, StringComparison.OrdinalIgnoreCase ) >= 0;
      }
   }
}