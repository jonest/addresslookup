﻿namespace AddressLookup.Specification
{
   public class OrSpecification<T> : CompositeSpecification<T> where T : class
   {
      private readonly ISpecification<T> _one;
      private readonly ISpecification<T> _other;

      public OrSpecification( ISpecification<T> one, ISpecification<T> other )
      {
         _one = one;
         _other = other;
      }

      public override bool IsSatisfiedBy( T candidate )
      {
         return _one.IsSatisfiedBy( candidate ) || _other.IsSatisfiedBy( candidate );
      }
   }
}