﻿namespace AddressLookup.Specification
{
   public abstract class CompositeSpecification<T> : ISpecification<T> where T : class
   {
      public abstract bool IsSatisfiedBy( T candidate );

      public ISpecification<T> And( ISpecification<T> other )
      {
         return new AndSpecification<T>( this, other );
      }

      public ISpecification<T> Or( ISpecification<T> other )
      {
         return new OrSpecification<T>( this, other );
      }

      ISpecification<T> ISpecification<T>.Not()
      {
         return new NotSpecification<T>( this );
      }
   }
}