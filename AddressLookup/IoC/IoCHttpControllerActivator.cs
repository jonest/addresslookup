﻿using System;
using System.Net.Http;
using System.Web.Http.Controllers;
using System.Web.Http.Dispatcher;
using Microsoft.Practices.Unity;

namespace AddressLookup.IoC
{
   public class IoCHttpControllerActivator : IHttpControllerActivator
   {
      public IHttpController Create( HttpRequestMessage request, HttpControllerDescriptor controllerDescriptor, Type controllerType )
      {
         return IoCUnityContainer.Container.Resolve( controllerType ) as IHttpController;
      }
   }
}