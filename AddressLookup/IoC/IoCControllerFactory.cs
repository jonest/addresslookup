﻿using System;
using System.Web.Mvc;
using System.Web.Routing;
using Microsoft.Practices.Unity;

namespace AddressLookup.IoC
{
   public class IoCControllerFactory : DefaultControllerFactory
   {
      protected override IController GetControllerInstance( RequestContext requestContext, Type controllerType )
      {
         try
         {
            if ( controllerType == null )
            {
               throw new ArgumentNullException( "controllerType" );
            }

            if ( !typeof( IController ).IsAssignableFrom( controllerType ) )
            {
               throw new ArgumentException( string.Format( "Type requested is not a controller: {0}", controllerType.Name ), "controllerType" );
            }

            return IoCUnityContainer.Container.Resolve( controllerType ) as IController;
         }
         catch
         {
            return null;
         }
      }
   }
}