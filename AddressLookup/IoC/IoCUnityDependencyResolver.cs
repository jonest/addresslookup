﻿using System;
using System.Collections.Generic;
using System.Web.Http.Dependencies;
using Microsoft.Practices.Unity;

namespace AddressLookup.IoC
{
   public class IoCUnityDependencyResolver : IDependencyResolver
   {
      private readonly IUnityContainer _container;

      public IoCUnityDependencyResolver( IUnityContainer container )
      {
         if ( container == null )
         {
            throw new ArgumentNullException( "container" );
         }

         _container = container;
      }

      public object GetService( Type serviceType )
      {
         try
         {
            return _container.Resolve( serviceType );
         }
         catch
         {
            return null;
         }
      }

      public IEnumerable<object> GetServices( Type serviceType )
      {
         try
         {
            return _container.ResolveAll( serviceType );
         }
         catch
         {
            return new List<object>();
         }
      }

      public IDependencyScope BeginScope()
      {
         IUnityContainer child = _container.CreateChildContainer();

         return new IoCUnityDependencyResolver( child );
      }

      public void Dispose()
      {
         _container.Dispose();
      }
   }
}