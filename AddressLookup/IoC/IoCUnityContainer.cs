﻿using Microsoft.Practices.Unity;

namespace AddressLookup.IoC
{
   public class IoCUnityContainer
   {
      public static UnityContainer Container { get; set; }
   }
}