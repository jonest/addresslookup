﻿using System.Web.Http.Dispatcher;
using AddressLookup.Repositories;
using Microsoft.Practices.Unity;

namespace AddressLookup.IoC
{
   public class IoCBootstrapper
   {
      public static IUnityContainer Initialize()
      {
         var container = new UnityContainer();

         container.RegisterType<IHttpControllerActivator, IoCHttpControllerActivator>();
         container.RegisterType<IUserRepository, UserRepository>();

         IoCUnityContainer.Container = container;

         return container;
      }
   }
}