﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using AddressLookup.Builders;
using AddressLookup.BussinessObjects;
using AddressLookup.Exceptions;
using AddressLookup.Repositories;
using AddressLookup.Specification;

namespace AddressLookup.Controllers.Api
{
   public class UserApiController : ApiController
   {
      private readonly IUserRepository _userRepository;

      public UserApiController( IUserRepository userRepository )
      {
         _userRepository = userRepository;
      }

      [HttpGet]
      [Route( "api/v/1/users", Name = "GetUsers" )]
      public IEnumerable<User> GetUsers()
      {
         return _userRepository.GetUsers();
      }

      [HttpGet]
      [Route( "api/v/1/users/search/{searchText}", Name = "SearchUsers" )]
      public IEnumerable<User> SearchUsers( string searchText )
      {
         return _userRepository.SearchUsers( searchText );
      }

      [HttpGet]
      [Route( "api/v/1/users/complex-search", Name = "ComplexSearchUsers" )]
      public IEnumerable<User> ComplexSearchUsers( string firstName = null, string lastName = null, string age = null, string picture = null, string interests = null, string street = null, string city = null, string state = null, string zip = null, string operand = null )
      {
         List<User> users = _userRepository.GetUsers().ToList();

         var userSpecBuilder = new UserSpecificationBuilder
         {
            FirstName = firstName,
            LastName = lastName,
            Age = age
         };

         IEnumerable<User> userMatches = GetMatches( operand, userSpecBuilder, users, user => user );

         var addresSpecBuilder = new AddressSpecificationBuilder
         {
            Street = street,
            City = city,
            State = state,
            Zip = zip
         };

         IEnumerable<User> addressMatches = GetMatches( operand, addresSpecBuilder, users, user => user.Address );

         var interestSpecBuilder = new InterestSpecificationBuilder
         {
            Name = interests
         };

         IEnumerable<User> interestMatches = GetMatches( operand, interestSpecBuilder, users, user => user.Interests.ToList() );

         return userMatches.Union( addressMatches ).Union( interestMatches ).ToList();
      }

      private static IEnumerable<User> GetMatches<T>( string operand, ISpecificationBuilder<T> specBuilder, IEnumerable<User> users, Func<User, T> func ) where T : class
      {
         ISpecification<T> spec;
         try
         {
            spec = specBuilder.Build( operand );
         }
         catch ( InvalidSpecException<T> )
         {
            return new List<User>();
         }

         return users.Where( u => spec.IsSatisfiedBy( func( u ) ) );
      }

      [HttpPost]
      [Route( "api/v/1/users", Name = "AddUser" )]
      public async Task<int> AddUser( [FromBody] AddUserRequest request )
      {
         if ( request == null )
         {
            throw new HttpResponseException( HttpStatusCodeExtension.UnprocessableEntity );
         }

         User user = BussinessObjects.User.FromAddUserRequest( request );

         return await _userRepository.AddUserAsync( user ).ConfigureAwait( false );
      }

      [HttpDelete]
      [Route( "api/v/1/users/{userId}", Name = "DeleteUser" )]
      public async Task<HttpResponseMessage> DeleteUser( string userId )
      {
         await _userRepository.DeleteUserAsync( userId ).ConfigureAwait( false );

         return new HttpResponseMessage( HttpStatusCode.NoContent );
      }
   }
}