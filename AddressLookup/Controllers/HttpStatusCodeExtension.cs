﻿using System.Net;

namespace AddressLookup.Controllers
{
   public class HttpStatusCodeExtension
   {
      private const int UnprocessableEntityInt = 422;

      public static HttpStatusCode UnprocessableEntity
      {
         get
         {
            return (HttpStatusCode) UnprocessableEntityInt;
         }
      }
   }
}