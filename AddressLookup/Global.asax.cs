﻿using System.Net.Http.Formatting;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using AddressLookup.IoC;
using Newtonsoft.Json.Serialization;

namespace AddressLookup
{
   public class MvcApplication : HttpApplication
   {
      protected void Application_Start()
      {
         AreaRegistration.RegisterAllAreas();
         FilterConfig.RegisterGlobalFilters( GlobalFilters.Filters );
         RouteConfig.RegisterRoutes( RouteTable.Routes );
         BundleConfig.RegisterBundles( BundleTable.Bundles );
         GlobalConfiguration.Configure( WebApiConfig.Register );

         IoCBootstrapper.Initialize();
         ControllerBuilder.Current.SetControllerFactory( typeof( IoCControllerFactory ) );

         HttpConfiguration config = GlobalConfiguration.Configuration;
         JsonMediaTypeFormatter jsonFormatter = config.Formatters.JsonFormatter;

         jsonFormatter.UseDataContractJsonSerializer = false;
         jsonFormatter.SerializerSettings.NullValueHandling = Newtonsoft.Json.NullValueHandling.Ignore;
         jsonFormatter.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();

      }
   }
}
