﻿namespace AddressLookup.BussinessObjects
{
   public class AddUserRequest
   {
      public string FirstName { get; set; }
      public string LastName { get; set; }
      public int Age { get; set; }
      public string PictureUrl { get; set; }
      public string Interests { get; set; }
      public AddAddressRequest Address { get; set; }
   }
}