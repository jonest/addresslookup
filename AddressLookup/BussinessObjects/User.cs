﻿using System;
using System.Collections.Generic;
using System.Linq;
using AddressLookup.Models;

namespace AddressLookup.BussinessObjects
{
   public class User
   {
      public int UserId { get; set; }

      public string FirstName { get; set; }

      public string LastName { get; set; }

      public int Age { get; set; }

      public Uri PictureUrl { get; set; }

      public IEnumerable<Interest> Interests { get; set; }

      public Address Address { get; set; }

      public static User FromUserEntity( UserEntity entity )
      {
         return new User
         {
            UserId = entity.UserId,
            FirstName = entity.FirstName,
            LastName = entity.LastName,
            Age = entity.Age,
            PictureUrl = new Uri( entity.PictureUrl ),
            Interests = entity.Interests.Select( Interest.FromeInterestEntity ).ToList(),
            Address = Address.FromAddressEntity( entity.Address )
         };
      }

      public static User FromAddUserRequest( AddUserRequest request )
      {
         return new User
         {
            Age = request.Age,
            FirstName = request.FirstName,
            LastName = request.LastName,
            PictureUrl = new Uri( request.PictureUrl ),
            Interests = request.Interests.Split( ',' ).Select( interest => new Interest
            {
               Name = interest.Trim()
            } ),
            Address = Address.FromAddAddressRequest( request.Address )
         };
      }
   }
}