﻿using AddressLookup.Models;

namespace AddressLookup.BussinessObjects
{
   public class Interest
   {
      public string Name { get; set; }

      public static Interest FromeInterestEntity( InterestEntity entity )
      {
         return new Interest
         {
            Name = entity.Name
         };
      }
   }
}