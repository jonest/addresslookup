﻿using AddressLookup.Models;

namespace AddressLookup.BussinessObjects
{
   public class Address
   {
      public string Line1 { get; set; }
      public string Line2 { get; set; }
      public string City { get; set; }
      public string State { get; set; }
      public string Zip { get; set; }

      public static Address FromAddressEntity( AddressEntity entity )
      {
         return new Address
         {
            Line1 = entity.Line1,
            Line2 = entity.Line2,
            City = entity.City,
            State = entity.State,
            Zip = entity.Zip
         };
      }

      public static Address FromAddAddressRequest( AddAddressRequest request )
      {
         return new Address
         {
            Line1 = request.Line1,
            Line2 = request.Line2,
            City = request.City,
            State = request.State,
            Zip = request.Zip
         };
      }
   }
}