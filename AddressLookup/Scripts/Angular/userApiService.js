﻿(function (window, angular) {
   "use strict";

   window.AddressLookup = window.AddressLookup || {};

   window.AddressLookup.userApiService = function ($http, $q) {
      var baseUrl = "/api/v/1/users";

      var remoteApiCall = function (params) {
         var deferred = $q.defer();

         $http(params)
            .success(function (data) {
               deferred.resolve(data);
            })
            .error(function (data, status) {
               var errorResponse = {
                  data: data,
                  status: status
               };
               deferred.reject(errorResponse);
            });

         return deferred.promise;
      }

      return {
         getUsers: function () {
            return remoteApiCall({
               method: "GET",
               url: baseUrl
            });
         },
         searchUsers: function (searchText) {
            return remoteApiCall({
               method: "GET",
               url: baseUrl + "/search/" + searchText
            });
         },
         addUser: function (user) {
            return remoteApiCall({
               method: "POST",
               url: baseUrl,
               data: user
            });
         },
         removeUser: function (userId) {
            return remoteApiCall(
            {
               method: "DELETE",
               url: baseUrl + "/" + userId
            });
         }
      }
   }

   try {
      angular.module("addressLookupApp")
         .service("userApiService", ["$http", "$q", window.AddressLookup.userApiService]);
   } catch (e) {
      // ignore
   }
}(window, angular));