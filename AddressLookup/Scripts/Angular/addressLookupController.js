﻿var addressLookupApp = angular.module("addressLookupApp", []);

addressLookupApp.controller("addressLookupController", ["$scope", "userApiService", function ($scope, userApiService) {
   $scope.addImageUrl = "";
   $scope.addFirstName = "";
   $scope.addLastName = "";
   $scope.addAge = "";
   $scope.addLine1 = "";
   $scope.addLine2 = "";
   $scope.addCity = "";
   $scope.addState = "";
   $scope.addZip = "";
   $scope.addInterests = "";

   var getAllUsers = function () {
      userApiService.getUsers().then(function (users) {
         $scope.users = users;
      }).catch(function (error) {
         console.log("Error received: ", error);
         $scope.users = {};
      });
   };

   $scope.searchUsers = function () {
      if ($scope.searchText.length === 0) {
         getAllUsers();
         return;
      }

      userApiService.searchUsers($scope.searchText).then(function (data) {
         $scope.users = data;
      }).catch(function (error) {
         console.log("Error received: ", error);
         $scope.users = {};
      });
   }

   $scope.removeUser = function (userId) {
      userApiService.removeUser(userId).then(function () {
         $scope.users = $scope.users.filter(function (user) {
            return user.userId !== userId;
         });
      }).catch(function (error) {
         console.log("Error received: ", error);
      });
   }

   var validAddParameters = function () {
      return $scope.addImageUrl !== ""
         && $scope.addFirstName !== ""
         && $scope.addLastName !== ""
         && !isNaN(parseInt($scope.addAge))
         && $scope.addLine1 !== ""
         && $scope.addCity !== ""
         && $scope.addState !== ""
         && $scope.addZip !== ""
         && $scope.addInterests !== "";
   }

   $scope.addUser = function () {
      if (!validAddParameters()) {
         return;
      }

      var addUserRequest = {
         firstName: $scope.addFirstName,
         lastName: $scope.addLastName,
         age: parseInt($scope.addAge),
         address: {
            line1: $scope.addLine1,
            line2: $scope.addLine2,
            city: $scope.addCity,
            state: $scope.addState,
            zip: $scope.addZip
         },
         interests: $scope.addInterests,
         pictureUrl: $scope.addImageUrl
      }

      userApiService.addUser(addUserRequest).then(function (userId) {
         var newUser = angular.copy(addUserRequest);
         newUser.userId = userId;
         newUser.interests = addUserRequest.interests.split(",").map(function (interest) {
            return {
               name: interest.trim()
            }
         });
         $scope.users.push(newUser);
      });
   }

   getAllUsers();
}]);