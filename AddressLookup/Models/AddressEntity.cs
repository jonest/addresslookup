﻿using System.ComponentModel.DataAnnotations;

namespace AddressLookup.Models
{
   public class AddressEntity
   {
      [Key]
      public int AddressId { get; set; }

      public string Line1 { get; set; }

      public string Line2 { get; set; }

      public string City { get; set; }

      public string State { get; set; }

      [DataType( DataType.PostalCode )]
      public string Zip { get; set; }
   }
}