﻿using System.ComponentModel.DataAnnotations;

namespace AddressLookup.Models
{
   public class InterestEntity
   {
      [Key]
      public int InterestId { get; set; }
      public string Name { get; set; }
   }
}