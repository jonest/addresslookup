﻿using System.Data.Entity;
using System.Diagnostics;

namespace AddressLookup.Models
{
   public class AddressLookupDbContext : DbContext
   {
      public AddressLookupDbContext() : base("name=AddressLookup")
      {
         Database.Log = message => Debug.WriteLine( message );
      }

      protected override void OnModelCreating( DbModelBuilder modelBuilder )
      {
         modelBuilder.Entity<UserEntity>().HasMany<InterestEntity>( u => u.Interests ).WithRequired().WillCascadeOnDelete();
      }

      public IDbSet<UserEntity> Users { get; set; }

      public IDbSet<AddressEntity> Addresses { get; set; }

      public IDbSet<InterestEntity> Interests { get; set; }
   }
}