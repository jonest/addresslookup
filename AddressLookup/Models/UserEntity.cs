﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace AddressLookup.Models
{
   public class UserEntity
   {
      [Key]
      public int UserId { get; set; }

      public string FirstName { get; set; }

      public string LastName { get; set; }

      public int Age { get; set; }

      [DataType( DataType.ImageUrl )]
      public string PictureUrl { get; set; }

      public virtual List<InterestEntity> Interests { get; set; }

      public virtual AddressEntity Address { get; set; }
   }
}