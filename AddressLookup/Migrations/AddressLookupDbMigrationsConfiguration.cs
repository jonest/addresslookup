using System.Collections.Generic;
using System.Data.Entity.Migrations;
using AddressLookup.Models;
using AddressLookup.Properties;
using Newtonsoft.Json;

namespace AddressLookup.Migrations
{
   public sealed class AddressLookupDbMigrationsConfiguration : DbMigrationsConfiguration<AddressLookupDbContext>
   {
      public AddressLookupDbMigrationsConfiguration()
      {
         AutomaticMigrationsEnabled = true;
         AutomaticMigrationDataLossAllowed = true;
      }

      protected override void Seed( AddressLookupDbContext context )
      {
         string userListJson = Resources.TestUsersJson;
         var userList = JsonConvert.DeserializeObject<List<UserEntity>>( userListJson );

         context.Users.AddOrUpdate( userList.ToArray() );
         context.SaveChanges();
      }
   }
}
