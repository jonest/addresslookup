namespace AddressLookup.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.AddressEntities",
                c => new
                    {
                        AddressId = c.Int(nullable: false, identity: true),
                        Line1 = c.String(),
                        Line2 = c.String(),
                        City = c.String(),
                        State = c.String(),
                        Zip = c.String(),
                    })
                .PrimaryKey(t => t.AddressId);
            
            CreateTable(
                "dbo.InterestEntities",
                c => new
                    {
                        InterestId = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        UserEntity_UserId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.InterestId)
                .ForeignKey("dbo.UserEntities", t => t.UserEntity_UserId, cascadeDelete: true)
                .Index(t => t.UserEntity_UserId);
            
            CreateTable(
                "dbo.UserEntities",
                c => new
                    {
                        UserId = c.Int(nullable: false, identity: true),
                        FirstName = c.String(),
                        LastName = c.String(),
                        Age = c.Int(nullable: false),
                        PictureUrl = c.String(),
                        Address_AddressId = c.Int(),
                    })
                .PrimaryKey(t => t.UserId)
                .ForeignKey("dbo.AddressEntities", t => t.Address_AddressId)
                .Index(t => t.Address_AddressId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.InterestEntities", "UserEntity_UserId", "dbo.UserEntities");
            DropForeignKey("dbo.UserEntities", "Address_AddressId", "dbo.AddressEntities");
            DropIndex("dbo.UserEntities", new[] { "Address_AddressId" });
            DropIndex("dbo.InterestEntities", new[] { "UserEntity_UserId" });
            DropTable("dbo.UserEntities");
            DropTable("dbo.InterestEntities");
            DropTable("dbo.AddressEntities");
        }
    }
}
