﻿using System;
using System.Data.Entity.Migrations.Infrastructure;
using System.Diagnostics;

namespace AddressLookup.Migrations
{
   public class DebugMigrationsLogger : MigrationsLogger
   {
      public override void Info( string message )
      {
         Debug.WriteLine( message );
      }

      public override void Warning( string message )
      {
         Debug.WriteLine( string.Format( "Warning: {0}", message ) );
      }

      public override void Verbose( string message )
      {
         Debug.WriteLine( message );
      }
   }
}