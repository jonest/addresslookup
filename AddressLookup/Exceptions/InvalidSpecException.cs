﻿using System;
using System.Runtime.Serialization;

namespace AddressLookup.Exceptions
{
   [Serializable]
   public class InvalidSpecException<T> : Exception where T : class 
   {
      public InvalidSpecException()
      {
      }

      public InvalidSpecException( string message )
         : base( message )
      {
      }

      public InvalidSpecException( string message, Exception innerException )
         : base( message, innerException )
      {
      }

      protected InvalidSpecException( SerializationInfo info, StreamingContext context )
         : base( info, context )
      {
      }
   }
}