﻿using System.Collections.Generic;
using AddressLookup.BussinessObjects;
using AddressLookup.Specification;
using AddressLookup.Specification.Address;

namespace AddressLookup.Builders
{
   public class AddressSpecificationBuilder : ISpecificationBuilder<Address>
   {
      public string Street { get; set; }
      public string City { get; set; }
      public string State { get; set; }
      public string Zip { get; set; }

      public ISpecification<Address> Build( string operand )
      {
         var specQueue = new Queue<ISpecification<Address>>();

         if ( !string.IsNullOrWhiteSpace( Street ) )
         {
            specQueue.Enqueue( new AddressLineSpecification( Street ) );
         }

         if ( !string.IsNullOrWhiteSpace( City ) )
         {
            specQueue.Enqueue( new CitySpecification( City ) );
         }

         if ( !string.IsNullOrWhiteSpace( State ) )
         {
            specQueue.Enqueue( new StateSpecification( State ) );
         }

         if ( !string.IsNullOrWhiteSpace( Zip ) )
         {
            specQueue.Enqueue( new ZipSpecification( Zip ) );
         }

         var operandBuilder = new OperandBuilder<Address>( operand );

         return operandBuilder.Build( specQueue );
      }
   }
}