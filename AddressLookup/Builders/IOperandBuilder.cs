﻿using System.Collections.Generic;
using AddressLookup.Specification;

namespace AddressLookup.Builders
{
   public interface IOperandBuilder<T> where T : class
   {
      /// <summary>
      /// Builds a chained specification
      /// </summary>
      /// <param name="queue">A queue of specifications to be chained</param>
      /// <returns>A single chained specification</returns>
      ISpecification<T> Build( Queue<ISpecification<T>> queue );
   }
}