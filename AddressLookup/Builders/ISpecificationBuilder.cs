﻿using AddressLookup.Specification;

namespace AddressLookup.Builders
{
   public interface ISpecificationBuilder<T> where T : class
   {
      /// <summary>
      /// Builds a specification
      /// </summary>
      /// <param name="operand">Logical operand for building the specification</param>
      /// <returns>A specification to test objects against</returns>
      ISpecification<T> Build( string operand );
   }
}
