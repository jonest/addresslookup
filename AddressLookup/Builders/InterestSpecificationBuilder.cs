﻿using System.Collections.Generic;
using AddressLookup.BussinessObjects;
using AddressLookup.Specification;
using AddressLookup.Specification.Interest;

namespace AddressLookup.Builders
{
   public class InterestSpecificationBuilder : ISpecificationBuilder<List<Interest>>
   {
      public string Name { get; set; }

      public ISpecification<List<Interest>> Build( string operand )
      {
         var specQueue = new Queue<ISpecification<List<Interest>>>();

         if ( !string.IsNullOrWhiteSpace( Name ) )
         {
            specQueue.Enqueue( new InterestSpecification( Name ) );
         }

         var operandBuilder = new OperandBuilder<List<Interest>>( operand );

         return operandBuilder.Build( specQueue );
      }
   }
}