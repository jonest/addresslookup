﻿using System;
using System.Collections.Generic;
using AddressLookup.BussinessObjects;
using AddressLookup.Specification;
using AddressLookup.Specification.User;

namespace AddressLookup.Builders
{
   public class UserSpecificationBuilder : ISpecificationBuilder<User>
   {
      public string FirstName { get; set; }

      public string LastName { get; set; }

      public string Age { get; set; }

      public string PictureUrl { get; set; }

      public string Interests { get; set; }

      public string Address { get; set; }

      public ISpecification<User> Build( string operand )
      {
         var specQueue = new Queue<ISpecification<User>>();

         if ( !string.IsNullOrWhiteSpace( FirstName ) )
         {
            specQueue.Enqueue( new FirstNameSpecification( FirstName ) );
         }

         if ( !string.IsNullOrWhiteSpace( LastName ) )
         {
            specQueue.Enqueue( new LastNameSpecification( LastName ) );
         }

         try
         {
            specQueue.Enqueue( new AgeSpecification( Convert.ToInt32( Age ) ) );
         }
         catch ( FormatException )
         {
         }
         catch ( OverflowException )
         {
         }

         var operandBuilder = new OperandBuilder<User>( operand );

         return operandBuilder.Build( specQueue );
      }
   }
}