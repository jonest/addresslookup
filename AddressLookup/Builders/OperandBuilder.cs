﻿using System;
using System.Collections.Generic;
using AddressLookup.Exceptions;
using AddressLookup.Specification;

namespace AddressLookup.Builders
{
   public class OperandBuilder<T> : IOperandBuilder<T> where T : class
   {
      private readonly Operand _operand;

      public OperandBuilder( string operand )
      {
         _operand = string.IsNullOrWhiteSpace( operand ) || operand.IndexOf( "or", StringComparison.OrdinalIgnoreCase ) >= 0 ? Operand.Or : Operand.And;
      }

      public ISpecification<T> Build( Queue<ISpecification<T>> queue )
      {
         if ( queue.Count == 0 )
         {
            throw new InvalidSpecException<T>();
         }

         ISpecification<T> compoundSpec = queue.Dequeue();

         switch ( _operand )
         {
            case Operand.Or:
            {
               while ( queue.Count > 0 )
               {
                  ISpecification<T> spec = queue.Dequeue();
                  compoundSpec = compoundSpec.Or( spec );
               }
               break;
            }
            case Operand.And:
            {
               while ( queue.Count > 0 )
               {
                  ISpecification<T> spec = queue.Dequeue();
                  compoundSpec = compoundSpec.And( spec );
               }
               break;
            }
         }

         return compoundSpec;
      }

      private enum Operand
      {
         Or,
         And
      }
   }
}