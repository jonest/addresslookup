﻿using System.Web.Http;
using AddressLookup.IoC;
using Microsoft.Practices.Unity;

namespace AddressLookup
{
   public static class WebApiConfig
   {
      public static void Register( HttpConfiguration config )
      {
         config.MapHttpAttributeRoutes();
         IUnityContainer container = IoCBootstrapper.Initialize();
         config.DependencyResolver = new IoCUnityDependencyResolver( container );
      }
   }
}