To seed the database, run `Update-Database` from the Package Manager Console in Visual Studio.

For testing JavaScript:

 - Install [Nodejs](https://nodejs.org/en/)
 - Install Karma
    - `npm install -g karma`
    - `npm install -g karma-cli`
    - run `karma start` to start karma test runner
 - Install Jasmine
    - `npm install -g jasmine-core`


API for complex search was an experimentation using a specification pattern to chain queries, combined with a builder to build the complete chained query.

Calls to this endpoint can be made like:

```HTTP
/api/v/1/users/complex-search?firstName={value}&lastName={value}&city={value}&operand=OR
```

Note that the `operand` is either `AND` or `OR`, where an `AND` uses logical `AND` for each data type, for example it will look for all users matching all the user queries OR all users matching the interest queries OR all the users matching the address queries, using a logical `OR` to join the different results.

Since this was a bit of an experimentation with a week long project, I did not fully flesh out this in it's entirely.