﻿using System.Collections.Generic;
using System.Linq;
using System.Net;
using AddressLookup.BussinessObjects;
using AddressLookup.Controllers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;
using RestSharp;

namespace AddressLookupTest.Integration.Controllers
{
   [TestClass]
   public class UserApiControllerIntegrationTest
   {
      private static RestClient _client;

      private const string FirstName = "Ty";
      private const string LastName = "Jones";
      private const int Age = 99;
      private const string PictureUrl = "http://test.test";

      private const string Interests = "interestName1, interestName2";

      private const string Line1 = "addressLine1";
      private const string Line2 = "addressLine2";
      private const string City = "addressCity";
      private const string State = "addressState";
      private const string Zip = "addressZip";

      [TestInitialize]
      public void Setup()
      {
         _client = new RestClient( "https://localhost:44300/" );
      }

      [TestMethod]
      [TestCategory( "Integration" )]
      public void GetUsers_ValidRequest_UsersReturned()
      {
         var request = new RestRequest( "/api/v/1/users", Method.GET );


         IRestResponse response = _client.Execute( request );


         Assert.AreEqual( HttpStatusCode.OK, response.StatusCode );

         var users = JsonConvert.DeserializeObject<List<User>>( response.Content );
         Assert.IsNotNull( users );
      }

      [TestMethod]
      [TestCategory( "Integration" )]
      public void AddUser_ValidRequest_UserIdReturned()
      {
         var addAddressRequest = new AddAddressRequest
         {
            Line1 = Line1,
            Line2 = Line2,
            City = City,
            State = State,
            Zip = Zip
         };

         var addUserRequest = new AddUserRequest
         {
            FirstName = FirstName,
            LastName = LastName,
            Age = Age,
            PictureUrl = PictureUrl,
            Interests = Interests,
            Address = addAddressRequest
         };

         var request = new RestRequest( "/api/v/1/users", Method.POST )
         {
            JsonSerializer = new JsonSerializer(),
            RequestFormat = DataFormat.Json
         };
         request.AddBody( addUserRequest );


         IRestResponse response = _client.Execute( request );


         Assert.AreEqual( HttpStatusCode.OK, response.StatusCode );

         var userId = JsonConvert.DeserializeObject<int>( response.Content );
         Assert.IsNotNull( userId );
      }

      [TestMethod]
      [TestCategory( "Integration" )]
      public void AddUser_InvalidRequest_UnprocessableEntityReturned()
      {
         var request = new RestRequest( "/api/v/1/users", Method.POST )
         {
            JsonSerializer = new JsonSerializer(),
            RequestFormat = DataFormat.Json
         };
         request.AddBody( null );


         IRestResponse response = _client.Execute( request );


         Assert.AreEqual( HttpStatusCodeExtension.UnprocessableEntity, response.StatusCode );
      }

      [TestMethod]
      [TestCategory( "Integration" )]
      public void SearchUsers_ValidRequest_UserReturned()
      {
         var getUsersRequest = new RestRequest( "/api/v/1/users", Method.GET );

         IRestResponse response = _client.Execute( getUsersRequest );

         User searchUser = JsonConvert.DeserializeObject<List<User>>( response.Content ).First();

         var searchRequest = new RestRequest( "/api/v/1/users/search/{searchText}", Method.GET );
         searchRequest.AddParameter( "searchText", searchUser.FirstName, ParameterType.UrlSegment );


         IRestResponse searchResponse = _client.Execute( searchRequest );


         Assert.AreEqual( HttpStatusCode.OK, searchResponse.StatusCode );

         var foundUsers = JsonConvert.DeserializeObject<List<User>>( searchResponse.Content );

         Assert.IsNotNull( foundUsers );
         Assert.IsTrue( foundUsers.Any( u => string.Equals( u.FirstName, searchUser.FirstName ) ) );
      }

      [TestMethod]
      [TestCategory( "Integration" )]
      public void Delete_ValidRequest_UserIsDeleted()
      {
         var addAddressRequest = new AddAddressRequest
         {
            Line1 = Line1,
            Line2 = Line2,
            City = City,
            State = State,
            Zip = Zip
         };

         var addUserRequest = new AddUserRequest
         {
            FirstName = FirstName,
            LastName = LastName,
            Age = Age,
            PictureUrl = PictureUrl,
            Interests = Interests,
            Address = addAddressRequest
         };

         var addRequest = new RestRequest( "/api/v/1/users", Method.POST )
         {
            JsonSerializer = new JsonSerializer(),
            RequestFormat = DataFormat.Json
         };
         addRequest.AddBody( addUserRequest );

         IRestResponse addResponse = _client.Execute( addRequest );

         var userId = JsonConvert.DeserializeObject<int>( addResponse.Content );

         var deleteRequest = new RestRequest( "/api/v/1/users/{userId}", Method.DELETE );
         deleteRequest.AddParameter( "userId", userId, ParameterType.UrlSegment );


         IRestResponse deleteResponse = _client.Execute( deleteRequest );


         Assert.AreEqual( HttpStatusCode.NoContent, deleteResponse.StatusCode );

         IRestResponse getResponse = _client.Execute( new RestRequest( "/api/v/1/users", Method.GET ) );

         var users = JsonConvert.DeserializeObject<List<User>>( getResponse.Content );
         Assert.IsFalse( users.Any( u => u.UserId == userId ) );

      }
   }
}
