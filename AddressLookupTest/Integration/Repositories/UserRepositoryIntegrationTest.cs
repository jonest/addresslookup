﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AddressLookup.BussinessObjects;
using AddressLookup.Repositories;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AddressLookupTest.Integration.Repositories
{
   [TestClass]
   public class UserRepositoryIntegrationTest
   {
      private static UserRepository _repository;
      private List<int> _userIdsAdded;

      private const string FirstName = "Ty";
      private const string LastName = "Jones";

      private const string Line2 = "addressLine1";
      private const string Line1 = "addressLine2";
      private const string City = "addressCity";
      private const string State = "addressState";
      private const string Zip = "addressZip";

      private const string InterestName = "interestName";

      [TestInitialize]
      public void Setup()
      {
         _repository = new UserRepository();
         _userIdsAdded = new List<int>();
      }

      [TestCleanup]
      public async void Cleanup()
      {
         foreach ( int userId in _userIdsAdded )
         {
            await _repository.DeleteUserAsync( userId.ToString() ).ConfigureAwait( false );
         }
      }

      [TestMethod]
      [TestCategory( "Integration" )]
      public void GetUsers_ReturnsUsers()
      {
         IEnumerable<User> users = _repository.GetUsers();

         Assert.IsNotNull( users );
      }

      [TestMethod]
      [TestCategory( "Integration" )]
      public async Task AddUserAsync_ValidUser_UserIsAdded()
      {
         var address = new Address
         {
            Line1 = Line2,
            Line2 = Line1,
            City = City,
            State = State,
            Zip = Zip
         };

         var interests = new List<Interest>
         {
            new Interest
            {
               Name = InterestName
            }
         };

         var user = new User
         {
            Address = address,
            Age = 10,
            FirstName = string.Format( "{0}_{1}", FirstName, Guid.NewGuid() ),
            LastName = LastName,
            PictureUrl = new Uri( "http://www.localhost:44300" ),
            Interests = interests
         };


         int userId = await _repository.AddUserAsync( user ).ConfigureAwait( false );

         _userIdsAdded.Add( userId );


         IEnumerable<User> users = _repository.GetUsers();

         Assert.IsTrue( users.Contains( user, new UserSame() ) );
      }

      [TestMethod]
      [TestCategory( "Integration" )]
      public async Task DeleteUserAsync_ValidUser_UserIsDeleted()
      {
         var address = new Address
         {
            Line1 = Line2,
            Line2 = Line1,
            City = City,
            State = State,
            Zip = Zip
         };

         var interests = new List<Interest>
         {
            new Interest
            {
               Name = InterestName
            }
         };

         var user = new User
         {
            Address = address,
            Age = 10,
            FirstName = string.Format( "{0}_{1}", FirstName, Guid.NewGuid() ),
            LastName = LastName,
            PictureUrl = new Uri( "http://www.localhost:44300" ),
            Interests = interests
         };


         int userId = await _repository.AddUserAsync( user ).ConfigureAwait( false );

         _userIdsAdded.Add( userId );

         var userComparer = new UserSame();
         User retrievedUser = _repository.GetUsers().Single( u => userComparer.Equals( u, user ) );


         await _repository.DeleteUserAsync( retrievedUser.UserId.ToString() ).ConfigureAwait( false );


         Assert.IsFalse( _repository.GetUsers().Any( u => userComparer.Equals( u, user ) ) );
      }

      [TestMethod]
      [TestCategory( "Integration" )]
      public async Task Search_ValidSearch_UserIsFound()
      {
         var address = new Address
         {
            Line1 = Line2,
            Line2 = Line1,
            City = City,
            State = State,
            Zip = Zip
         };

         var interests = new List<Interest>
         {
            new Interest
            {
               Name = InterestName
            }
         };

         var user = new User
         {
            Address = address,
            Age = 10,
            FirstName = string.Format( "{0}_{1}", FirstName, Guid.NewGuid() ),
            LastName = LastName,
            PictureUrl = new Uri( "http://www.localhost:44300" ),
            Interests = interests
         };


         int userId = await _repository.AddUserAsync( user ).ConfigureAwait( false );

         _userIdsAdded.Add( userId );


         IEnumerable<User> retrievedUsers = _repository.SearchUsers( user.FirstName );


         Assert.IsTrue( retrievedUsers.Contains( user, new UserSame() ) );
      }

      #region EqualityComparers
      private class InterestSame : IEqualityComparer<Interest>
      {
         public bool Equals( Interest x, Interest y )
         {
            return x.Name == y.Name;
         }

         public int GetHashCode( Interest obj )
         {
            return obj.Name.GetHashCode();
         }
      }

      private class AddressSame : IEqualityComparer<Address>
      {
         public bool Equals( Address x, Address y )
         {
            bool line1Same = x.Line1 == y.Line1;
            bool line2Same = x.Line2 == y.Line2;
            bool citySame = x.City == y.City;
            bool stateSame = x.State == y.State;
            bool zipSame = x.Zip == y.Zip;

            return line1Same && line2Same && citySame && stateSame && zipSame;
         }

         public int GetHashCode( Address obj )
         {
            var stringBuilder = new StringBuilder();
            stringBuilder.Append( obj.Line1 );
            stringBuilder.Append( obj.Line2 );
            stringBuilder.Append( obj.City );
            stringBuilder.Append( obj.State );
            stringBuilder.Append( obj.Zip );

            return stringBuilder.ToString().GetHashCode();
         }
      }

      private class UserSame : IEqualityComparer<User>
      {
         public bool Equals( User x, User y )
         {
            bool firstSame = x.FirstName == y.FirstName;
            bool lastSame = x.LastName == y.LastName;
            bool ageSame = x.Age == y.Age;
            bool pictureSame = x.PictureUrl.AbsoluteUri == y.PictureUrl.AbsoluteUri;

            bool interestsSame = x.Interests.SequenceEqual( y.Interests, new InterestSame() );

            var addressComparer = new AddressSame();
            bool addressSame = addressComparer.Equals( x.Address, y.Address );

            return firstSame && lastSame && ageSame && pictureSame && interestsSame && addressSame;
         }

         public int GetHashCode( User obj )
         {
            var stringBuilder = new StringBuilder();
            stringBuilder.Append( obj.FirstName );
            stringBuilder.Append( obj.LastName );
            stringBuilder.Append( obj.Age );
            stringBuilder.Append( obj.PictureUrl.AbsoluteUri );

            var interestComparer = new InterestSame();
            foreach ( Interest interest in obj.Interests )
            {
               stringBuilder.Append( interestComparer.GetHashCode( interest ) );
            }

            var addressComperer = new AddressSame();
            stringBuilder.Append( addressComperer.GetHashCode( obj.Address ) );

            return stringBuilder.ToString().GetHashCode();
         }
      }
      #endregion
   }
}
