﻿describe("Service: userApiService", function () {
   var userApiService, httpBackend, q, rootScope;

   var expectedUsers = [
      {
         firstName: "Secret",
         lastName: "Squirrel",
         age: 21,
         pictureUrl: "https://test.com/test.jpg",
         interests: [
            {
               name: "tesing"
            },
            {
               name: " more testing"
            }
         ],
         address: {
            line1: "1234 Main St",
            line2: "Apt 123",
            city: "Squaresville",
            state: "MI",
            zip: "48864"
         }
      }
   ];

   beforeEach(module("addressLookupApp"));

   beforeEach(inject(function ($rootScope) {
      rootScope = $rootScope;
   }));

   beforeEach(inject(function (_userApiService_, $httpBackend, $q) {
      userApiService = _userApiService_;
      httpBackend = $httpBackend;
      q = $q;
   }));

   it("Should return users when getUsers called", function () {
      httpBackend.whenGET("/api/v/1/users").respond({
         data:
            expectedUsers
      });

      var actualUsers;
      userApiService.getUsers().then(function (response) {
         actualUsers = response.data;
      });

      httpBackend.flush();

      expect(actualUsers).toEqual(expectedUsers);
   });

   it("Should return users when searchUsers called", function () {
      httpBackend.whenGET("/api/v/1/users/search/squirrel").respond({
         data:
            expectedUsers
      });

      var actualUsers;
      userApiService.searchUsers("squirrel").then(function (response) {
         actualUsers = response.data;
      });

      httpBackend.flush();

      expect(actualUsers).toEqual(expectedUsers);
   });

   var expectedUserId = "123";
   it("Should return userId when addUser called", function () {
      httpBackend.whenPOST("/api/v/1/users", expectedUsers[0]).respond({
         data:
            expectedUserId
      });

      var actualUserId;
      userApiService.addUser(expectedUsers[0]).then(function (response) {
         actualUserId = response.data;
      });

      httpBackend.flush();

      expect(actualUserId).toEqual(expectedUserId);
   });

   var expectedStatusCode = 204;
   it("Should return success when deleteUser called", function () {
      httpBackend.whenDELETE("/api/v/1/users/123").respond(expectedStatusCode);

      var testPassed = false;
      userApiService.removeUser("123").then(function () {
         testPassed = true;
      });

      httpBackend.flush();

      expect(testPassed).toEqual(true);
   });
});