﻿describe("Controller: addressLookupController", function () {
   var addressLookupController, scope, mockUserApiService;

   var expectedUserId = "123";

   var expectedUsers = [
      {
         firstName: "Secret",
         lastName: "Squirrel",
         age: 21,
         pictureUrl: "https://test.com/test.jpg",
         interests: [
            {
               name: "tesing"
            },
            {
               name: " more testing"
            }
         ],
         address: {
            line1: "1234 Main St",
            line2: "Apt 123",
            city: "Squaresville",
            state: "MI",
            zip: "48864"
         },
         userId: expectedUserId
      }
   ];

   var expectedAddUserRequest = {
      firstName: "Secret",
      lastName: "Squirrel",
      age: 21,
      address: {
         line1: "1234 Main St",
         line2: "Apt 123",
         city: "Squaresville",
         state: "MI",
         zip: "48864"
      },
      interests: "test1,test2",
      pictureUrl: "https://test.com/test.jpg"
   }

   beforeEach(module("addressLookupApp"));

   beforeEach(module(function ($provide) {
      $provide.factory("userApiService", ["$http", "$q", function ($http, $q) {
         var promiseFunction = function (data) {
            var deferred = $q.defer();
            deferred.resolve(data);
            return deferred.promise;
         };

         return {
            getUsers: function () {
               return promiseFunction(expectedUsers);
            },
            searchUsers: function (searchText) {
               return promiseFunction(expectedUsers);
            },
            addUser: function (addUserRequest) {
               return promiseFunction(expectedUserId);
            },
            removeUser: function(userId) {
               return promiseFunction();
            }
         }
      }
      ]);
   }));

   beforeEach(inject(function ($rootScope, $controller, userApiService) {
      scope = $rootScope.$new();
      mockUserApiService = userApiService;
      spyOn(mockUserApiService, "searchUsers").and.callThrough();
      spyOn(mockUserApiService, "addUser").and.callThrough();
      spyOn(mockUserApiService, "removeUser").and.callThrough();
      addressLookupController = $controller("addressLookupController", {
         $scope: scope,
         userApiService: mockUserApiService
      });
   }));

   describe("When creating addressLookupController", function () {
      it("Should have scope values defined", function () {
         expect(scope.searchUsers).toBeDefined();
         expect(scope.removeUser).toBeDefined();
         expect(scope.addUser).toBeDefined();
         expect(scope.addImageUrl).toBeDefined();
         expect(scope.addFirstName).toBeDefined();
         expect(scope.addLastName).toBeDefined();
         expect(scope.addAge).toBeDefined();
         expect(scope.addLine1).toBeDefined();
         expect(scope.addLine2).toBeDefined();
         expect(scope.addCity).toBeDefined();
         expect(scope.addState).toBeDefined();
         expect(scope.addZip).toBeDefined();
         expect(scope.addInterests).toBeDefined();
      });
   });

   describe("When searching users", function () {
      it("Should call userApiService.searchUsers and scope.users should be updated", function () {
         scope.searchText = "test";


         scope.searchUsers();
         scope.$digest();


         expect(mockUserApiService.searchUsers).toHaveBeenCalledWith("test");
         expect(scope.users).toBe(expectedUsers);
      });
   });

   describe("When adding users", function () {
      it("Should call userApiService.addUser and add user to scope.users", function () {
         scope.addImageUrl = expectedAddUserRequest.pictureUrl;
         scope.addFirstName = expectedAddUserRequest.firstName;
         scope.addLastName = expectedAddUserRequest.lastName;
         scope.addAge = expectedAddUserRequest.age.toString();
         scope.addLine1 = expectedAddUserRequest.address.line1;
         scope.addLine2 = expectedAddUserRequest.address.line2;
         scope.addCity = expectedAddUserRequest.address.city;
         scope.addState = expectedAddUserRequest.address.state;
         scope.addZip = expectedAddUserRequest.address.zip;
         scope.addInterests = expectedAddUserRequest.interests;


         scope.addUser();
         scope.$digest();


         expect(mockUserApiService.addUser).toHaveBeenCalledWith(expectedAddUserRequest);

         var expectedListAfterAdd = function () {
            var newUser = expectedAddUserRequest;
            newUser.userId = expectedUserId;
            newUser.interests = expectedAddUserRequest.interests.split(",").map(function (interest) {
               return {
                  name: interest.trim()
               }
            });

            expectedUsers.push(newUser);

            return expectedUsers;
         };
         expect(scope.users).toBe(expectedListAfterAdd());
      });
   });

   describe("When removing users", function () {
      it("Should call userApiService.removeUser and user should be removed", function () {
         scope.removeUser(expectedUserId);
         scope.$digest();


         expect(mockUserApiService.removeUser).toHaveBeenCalledWith(expectedUserId);
         expect(scope.users.length).toBe(0);
      });
   });
});