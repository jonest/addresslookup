﻿using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using AddressLookup.BussinessObjects;
using AddressLookup.Controllers.Api;
using AddressLookup.Repositories;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace AddressLookupTest.Unit.Controllers
{
   [TestClass]
   public class UserApiControllerUnitTest
   {
      private UserApiController _controller;
      private IUserRepository _userRepository;
      private const string FirstName = "Ty";
      private const string LastName = "Jones";

      [TestInitialize]
      public void Setup()
      {
         _userRepository = Mock.Of<IUserRepository>();

         _controller = new UserApiController( _userRepository );
      }

      [TestMethod]
      [TestCategory( "Unit" )]
      public void GetUsers_ValidRequest_ReturnsHttpOk()
      {
         var users = new List<User>
         {
            new User
            {
               FirstName = FirstName,
               LastName = LastName
            }
         };
         Mock.Get( _userRepository ).Setup( repo => repo.GetUsers() ).Returns( users );


         IEnumerable<User> response = _controller.GetUsers();


         Assert.IsNotNull( response );
         Assert.IsTrue( response.Any( user => user.FirstName == FirstName && user.LastName == LastName ) );
      }

      [TestMethod]
      [TestCategory( "Unit" )]
      public void SearchUsers_ValidRequest_ReturnsCorrectUsers()
      {
         var users = new List<User>
         {
            new User
            {
               FirstName = FirstName,
               LastName = LastName
            }
         };
         Mock.Get( _userRepository ).Setup( repo => repo.SearchUsers( FirstName ) ).Returns( users );


         IEnumerable<User> response = _controller.SearchUsers( FirstName );


         Assert.IsNotNull( response );
         Assert.IsTrue( response.Any( user => user.FirstName == FirstName && user.LastName == LastName ) );
      }

      [TestMethod]
      [TestCategory( "Unit" )]
      public async Task AddUser_ValidRequest_ReturnsHttpAccepted()
      {
         var request = new AddUserRequest
         {
            FirstName = FirstName,
            LastName = LastName,
            PictureUrl = "http://localhost:44300",
            Address = new AddAddressRequest(),
            Interests = "test1, test2"
         };

         Mock.Get( _userRepository )
            .Setup( repo => repo.AddUserAsync( It.Is<User>( user => user.FirstName == FirstName && user.LastName == LastName ) ) )
            .Returns( Task.FromResult( 1 ) );


         int response = await _controller.AddUser( request ).ConfigureAwait( false );


         Assert.AreEqual( 1, response );
      }

      [TestMethod]
      [TestCategory( "Unit" )]
      public async Task DeleteUser_ValidRequest_ReturnsHttpAccepted()
      {
         HttpResponseMessage response = await _controller.DeleteUser( "1" ).ConfigureAwait( false );


         Assert.AreEqual( HttpStatusCode.NoContent, response.StatusCode );
      }
   }
}
