﻿using System.Data.Entity.Migrations;
using System.Data.Entity.Migrations.Infrastructure;
using AddressLookup.Migrations;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AddressLookupTest
{
   [TestClass]
   public class MigrateDbTest
   {
      [TestMethod]
      public void AddressLookupDbMigration_Update_MigrateIsSuccessful()
      {
         var config = new AddressLookupDbMigrationsConfiguration
         {
            AutomaticMigrationDataLossAllowed = true
         };

         var migrator = new DbMigrator( config );
         var loggingDecorator = new MigratorLoggingDecorator( migrator, new DebugMigrationsLogger() );
         loggingDecorator.Update();
      }
   }
}
